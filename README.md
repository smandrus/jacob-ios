# The Assignment #

Use the Giphy API to create an application which allows users to search, view, and favourite gifs.

## Specifications ##

A tab bar with 2 tabs - feed and favourites

## First Tab ##
* Contains a search bar at the top.
* Contains a table view that displays searched gifs.
* Loading indicator while searching
* The default items in the table view should be the trending gifs (if nothing in search bar)

## Second Tab ##
* Contains a collection view that displays a grid of favourited gifs stored locally.

## Gif Table View Cell ##
* Should contain a view of the gif running.
* Should contain a favourite button which allows favouriting and unfavouriting.
* The favourited items should be stored locally on the device.

## Gif Collection View Cell ##
* Should contain a view of the gif running.
* Should contain an unfavourite button

## Bonus ##
* Use iOS MVVM architecture instead of MVC
* Use RxSwift
* Add pagination (infinite scrolling) to the gif lists.


# The Solution #

## Notes ##
* Built in Swift 4, for iOS 10.3 running Xcode 9 beta 2. Swift 4 largely because I wanted to play with  `Codable` for the JSON, there are few known issues with it, and I assume you have Xcode 9 betas/Swift snapshots kicking around. iOS 10.3 because iOS 11 has plenty of known issues.

## Bugs ##
* When searching, a space is at the top of the table view. I have a feeling this is related to the new layout guides in iOS 11, as I tested two apps that worked under iOS 10 and they both exhibit the same behaviour (including Apple sample code). This bug doesn't exist in iOS 11. I don't have an iOS 10 device to test on.
*

## Giphy API ##
* I went a little overkill on the API, but it was my first opportunity to play with `Codable`.

## GIF View ##
* I used the popular `FLAnimatedImage` from Flipboard because I know it works well. It also demoes at least some comfortablility with Objective-C.
* I'd like to try out a looping mp4 to see how that performs instead.

## Table View ##
* Double tap a cell to favourite
* Pagination tries to stay 1 page ahead. Scrolling at normal speeds gives the appearance of an infinite list, but you can beat it if you try. I thought about being more clever with this, but I think it works well enough with a loading indicator at the end of the list.

## Collection View ##
* Double tap a cell to unfavourite

## Persistence Model ##
* I'm currently just writing the gifs to disk, and loading them back on launch. Perhaps a slightly more complex model would be useful (e.g. date favourited), but as you favourite hundreds, it doesn't seem useful to have them sorted in any particular order as you would have to scroll through an enormous list anyway.
