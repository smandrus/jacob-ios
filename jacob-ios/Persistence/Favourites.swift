//
//  File.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-25.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation
import AVKit

// File Manager for favourites
class Favourites {
    static let shared = Favourites()
    private let fileManager = FileManager.default
    private var favouritesURL: URL { return fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("Favourites") }
    
    private(set) var gifs: [URL] = []
    private init() {
        // Make sure Favourites folder exists
        // Should only fail if folder already exists. In which case we can ignore the error
        try? fileManager.createDirectory(at: favouritesURL, withIntermediateDirectories: false, attributes: nil)
            
        self.gifs = loadGIFs()
    }
    
    var didAddGif: (() -> Void)?
    
    private func loadGIFs() -> [URL] {
        // TODO: Deal with Error!
        // TODO: Sort by creation date
        // Load GIFs
        var gifs: [URL] = []
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: favouritesURL, includingPropertiesForKeys: [.contentModificationDateKey], options: [])
            print("File URLs:", fileURLs)
            gifs = fileURLs
        } catch {
            debugPrint(error)
        }
        
        return gifs
    }
    
    private func gifName(from url: URL) -> String {
        let components = url.pathComponents
        let name = components[components.endIndex - 2]
        return name
    }
    
    func addFavourite(url: URL, asset: AVAsset) {
        guard asset.isExportable else {
            fatalError()
        }
        
        // TODO: Why doesn't this work?
        // Persist file with name of GIF
        let name = gifName(from: url)
        let filePath = createFilePath(for: name).appendingPathExtension("mp4")
        
        // Don't save again if GIF already exists
        if gifs.contains(filePath) { return }
        
        let composition = AVMutableComposition()
        let compositionVideoTrack = composition.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID(kCMPersistentTrackID_Invalid))!
        
        let sourceVideoTrack = asset.tracks(withMediaType: .video).first!
        do {
            try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), of: sourceVideoTrack, at: kCMTimeZero)
        } catch {
           // completion(nil)
            print("1")
            return
        }
        
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: composition)
        var preset: String = AVAssetExportPresetPassthrough
//        if compatiblePresets.contains(AVAssetExportPreset1920x1080) { preset = AVAssetExportPreset1920x1080 }
        
        guard
            let exportSession = AVAssetExportSession(asset: composition, presetName: preset),
            exportSession.supportedFileTypes.contains(.mp4) else {
//                completion(nil)
                return
        }
        
        var tempFileUrl = favouritesURL.appendingPathComponent(name, isDirectory: false).appendingPathExtension(".mp4")
        tempFileUrl = URL(fileURLWithPath: tempFileUrl.path)
        
        exportSession.outputURL = tempFileUrl
        exportSession.outputFileType = .mp4
        let startTime = CMTimeMake(0, 1)
        let timeRange = CMTimeRangeMake(startTime, asset.duration)
        exportSession.timeRange = timeRange
        
        exportSession.exportAsynchronously {
            self.gifs.append(url)
        }
    }
    
    func removeFavourite(indexPath: IndexPath) {
        let index = indexPath.row
        let url = gifs[index]
        do {
            try fileManager.removeItem(at: url)
            gifs.remove(at: index)
        } catch {
            debugPrint(error)
        }
    }
    
    func removeFavourite(url: URL) {
        let name = gifName(from: url)
        let filePath = createFilePath(for: name)
        let index = gifs.index(of: filePath)!
        removeFavourite(indexPath: IndexPath(row: index, section: 0))
    }
    
    private func createFilePath(for name: String) -> URL {
        let filePath = favouritesURL.appendingPathComponent(name)
        return filePath
    }
}

