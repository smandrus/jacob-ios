//
//  GiphyCollectionViewModel.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-25.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation
import AVKit

struct GiphyCollectionViewControllerModel {
    
    private var favouriteGIFs: [URL] { return favourites.gifs }
    let favourites = Favourites.shared
    
    // Datasource
    
    var count: Int { return favouriteGIFs.count }
    func gif(for indexPath: IndexPath) -> AVPlayerItem {
        let url = favouriteGIFs[indexPath.row]
        let item = AVPlayerItem(url: url)
        print(item.tracks)
        return item
        
    }
    
    func unfavourite(for indexPath: IndexPath) {
        favourites.removeFavourite(indexPath: indexPath)
    }
}
