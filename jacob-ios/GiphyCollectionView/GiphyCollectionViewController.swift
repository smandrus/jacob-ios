//
//  GiphyCollectionViewController.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-25.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

class GiphyCollectionViewController: UICollectionViewController {
    typealias Model = GiphyCollectionViewControllerModel
    
    var model = Model()
    private let spacing: CGFloat = 1
    
    var previewingContext: UIViewControllerPreviewing?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell classes
        let nib = UINib(nibName: MP4CollectionViewCell.nibName, bundle: nil)
        self.collectionView!.register(nib, forCellWithReuseIdentifier: MP4CollectionViewCell.reuseIdentifier)

        // Do any additional setup after loading the view.
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let width = (collectionView!.frame.width / 2) - (spacing / 2)
        let height: CGFloat = 200
        layout.itemSize = CGSize(width: width, height: height)
        
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        
        // Check for force touch feature, and add force touch/previewing capability.
        if traitCollection.forceTouchCapability == .available {
            /*
             Register for `UIViewControllerPreviewingDelegate` to enable
             "Peek" and "Pop".
             (see: FoodListViewController+UIViewControllerPreviewing.swift)
             
             The view controller will be automatically unregistered when it is
             deallocated.
             */
            previewingContext = registerForPreviewing(with: self, sourceView: self.collectionView!)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.reloadData()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return model.count
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "String", for: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MP4CollectionViewCell.reuseIdentifier, for: indexPath) as! MP4CollectionViewCell
        
        // Configure the cell
        let gif = model.gif(for: indexPath)
        configure(cell: cell, gif: gif)
        
        return cell
    }
    
    func configure(cell: MP4CollectionViewCell, gif: AVPlayerItem) {
        cell.setMovie(gif)
        cell.playerView.videoGravity = .resizeAspectFill
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didUnfavouriteCell))
        doubleTapRecognizer.numberOfTapsRequired = 2
        cell.addGestureRecognizer(doubleTapRecognizer)
    }
    
    // CollectionView Delegate
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as? MP4CollectionViewCell)?.play()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as? MP4CollectionViewCell)?.pause()
    }
    
    @objc
    func didUnfavouriteCell(_ gestureRecognizer: UIGestureRecognizer) {
        let cell = gestureRecognizer.view as! MP4CollectionViewCell
        let indexPath = collectionView!.indexPath(for: cell)!
        cell.animateUnfavourite { (finished) in
            self.model.unfavourite(for: indexPath)
            self.collectionView?.deleteItems(at: [indexPath])
        }
    }
}

extension GiphyCollectionViewController: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let gif = model.gif(for: indexPath)
//        let size = gif.asset.tracks(withMediaType: .video).first!.naturalSize
//        let width = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize.width
//        let height = (size.height / size.width) * width
//        return CGSize(width: width, height: height)
//    }
}
