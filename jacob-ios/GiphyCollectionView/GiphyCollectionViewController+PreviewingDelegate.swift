//
//  GiphyCollectionViewController+PreviewingDelegate.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-25.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

extension GiphyCollectionViewController: UIViewControllerPreviewingDelegate {
    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        // Obtain the index path and the cell that was pressed.
        guard let indexPath = collectionView?.indexPathForItem(at: location),
            let cell = collectionView?.cellForItem(at: indexPath) as? MP4CollectionViewCell else { return nil }
        
        // Get GIF for selected cell
        let gif = model.gif(for: indexPath)
        let view = AVPlayerView()
        let player = AVQueuePlayer()
        view.player = player
        _ = AVPlayerLooper(player: player, templateItem: gif)
        
        let vc = UIViewController()
        vc.view = view
        
        // Set preview size
        vc.preferredContentSize = gif.asset.tracks(withMediaType: .video).first!.naturalSize
        
        // Set the source rect to the cell frame, so surrounding elements are blurred.
        previewingContext.sourceRect = cell.frame
        
        return vc
    }
    
    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        // Intentionally empty
        return
    }
    
    
}
