//
//  Giphy+GIF.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation

// TODO: Maybe implement "stills". Seems unneccessary for the task at hand.

extension Giphy {
    /// Represents the gif data from an api call.
    struct GIF: Decodable {
        /// The user discretion rating of a gif.
        enum Rating: String, Decodable {
            case Y = "y"
            case G = "g"
            case PG = "pg"
            case PG13 = "pg-13"
            case R = "r"
        }
        
        let url: URL
        let id: String
        let rating: Rating
        
        // MARK: - Image Metadata
        
        /// The original gif image.
        let originalMetadata: ImageMetadata
        /// The gif with a fixed height of 200px.
        let fixedHeightMetadata: ImageMetadata
        /// The gif with a fixed height of 200px downsampled.
        let fixedHeightDownsampledMetadata: ImageMetadata
        /// The gif with a fixed width of 200px.
        let fixedWidthMetadata: ImageMetadata
        /// The gif with a fixed width of 200px downsampled.
        let fixedWidthDownsampledMetadata: ImageMetadata
        
        private enum CodingKeys: String, CodingKey {
            case url
            case id
            case rating
            case imagesMetadata = "images"
        }
        
        private enum ImageMetadataKeys: String, CodingKey {
            case fixedHeight = "fixed_height"
            case fixedHeightDownsampled = "fixed_height_downsampled"
            case fixedWidth = "fixed_width"
            case fixedWidthDownsampled = "fixed_width_downsampled"
            case original
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            url = try values.decode(URL.self, forKey: .url)
            id = try values.decode(String.self, forKey: .id)
            let rating = try values.decode(String.self, forKey: .rating)
            self.rating = Rating(rawValue: rating)!
            
            // Image Metadata
            let imageMetadata = try values.nestedContainer(keyedBy: ImageMetadataKeys.self, forKey: .imagesMetadata)
            fixedHeightMetadata = try imageMetadata.decode(ImageMetadata.self, forKey: .fixedHeight)
            fixedHeightDownsampledMetadata = try imageMetadata.decode(ImageMetadata.self, forKey: .fixedHeightDownsampled)
            fixedWidthMetadata = try imageMetadata.decode(ImageMetadata.self, forKey: .fixedWidth)
            fixedWidthDownsampledMetadata = try imageMetadata.decode(ImageMetadata.self, forKey: .fixedWidthDownsampled)
            originalMetadata = try imageMetadata.decode(ImageMetadata.self, forKey: .original)
        }
    }
}



extension Giphy.GIF {
    struct ImageMetadata: Codable {
        /// The url of the gif.
        let url: URL
        
        /// The width of the gif in pixels.
        let width: Int
        
        /// The height of the gif in pixels
        let height: Int
        
        /// The size of the gif in bytes, not all image versions include this.
        let size: Int?
        
        /// The number of frames the gif has, not all image verions include this.
        let frames: Int?
        
        /// URL to the gif in mp4 format, all image verions include this except for stills.
        let mp4: URL?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let url = try values.decode(String.self, forKey: .url)
            self.url = URL(string: url)!
            
            let width = try values.decode(String.self, forKey: .width)
            self.width = Int(width)!
            
            let height = try values.decode(String.self, forKey: .height)
            self.height = Int(height)!
            
            if let size = try? values.decode(String.self, forKey: .size) {
                self.size = Int(size)
            } else {
                self.size = nil
            }
            
            if let frames = try? values.decode(String.self, forKey: .frames) {
                self.frames = Int(frames)
            } else {
                self.frames = nil
            }
            
            
            if let mp4 = try? values.decode(String.self, forKey: .mp4) {
                self.mp4 = URL(string: mp4)
            } else {
                self.mp4 = nil
            }
        }
    }
}

extension Giphy.GIF.Rating: CustomStringConvertible {
    
    public var description: String {
        return rawValue
    }
}
