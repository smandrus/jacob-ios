//
//  GiphyResponse+Metadata.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation

extension Giphy.Response {
    struct Metadata: Decodable {
        let message: String
        let responseID: String
        let status: Int
        
        private enum CodingKeys: String, CodingKey {
            case message = "msg"
            case responseID = "response_id"
            case status
        }
        
    }
}

extension Giphy.Response.Metadata: CustomStringConvertible {
    var description: String {
        return """
        
        """
    }
}
