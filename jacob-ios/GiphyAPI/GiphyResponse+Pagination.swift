//
//  GiphyResponse+Pagination.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation

extension Giphy.Response {
    struct Pagination: Decodable {
        /// The number of items retrieved
        let count: Int
        /// Offset of the first item in the list
        let offset: Int
        /// The total number of items found
        let totalCount: Int?
        
        private enum CodingKeys: String, CodingKey {
            case count
            case offset
            case totalCount = "total_count"
        }
    }
    
}
//extension Giphy.Response.Pagination: CustomStringConvertible {
//    var description: String {
//        return """
//        \(count)
//        \(offset)
//        \(totalCount)
//        """
//    }
//}

