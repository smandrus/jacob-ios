//
//  Giphy.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation

let giphyUserAPIKey = "cb997ba2ac0d4836a554e480a77b8d61" // Public Beta: "dc6zaTOxFJmzC"

// TODO: Maybe add struct for `params`?
struct Giphy {
    struct Response: Decodable {
        let pagination: Pagination?
        let meta: Metadata
        let data: [GIF]
    }
    
    static let baseURLString = "http://api.giphy.com/v1/gifs"
    
    /// The user's API key
    let apiKey: String
    
    /// The URLSession to use to communicate with Giphy
    let session: URLSession
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - apiKey: <#apiKey description#>
    ///   - sessionConfiguration: <#sessionConfiguration description#>
    init(apiKey: String, sessionConfiguration: URLSessionConfiguration = .default) {
        self.apiKey = apiKey
        self.session = URLSession(configuration: sessionConfiguration)
    }
    
    
    /// Perform a request for a gif by its Giphy id
    ///
    /// - Parameters:
    ///   - id: The id of the Giphy gif
    ///   - completionHandler: Completion handler called once the request is complete
    /// - Returns: A URLSessionDataTask for the request that has already been resumed
    @discardableResult func gif(_ id: String, completionHandler: @escaping (GIF?, Error?) -> Void) -> URLSessionDataTask {
        
        return performRequest(id, params: nil) {
            completionHandler($0?.first,$2)
        }
    }
    
    /// Perform a request for multiple gifs by id
    ///
    /// - Parameters:
    ///   - id: An array of Giphy gif ids
    ///   - completionHandler: Completion handler called once the request is complete
    /// - Returns: A URLSessionDataTask for the request that has already been resumed
    @discardableResult func gifs(_ ids: [String], completionHandler: @escaping ([GIF]?, Error?) -> Void) -> URLSessionDataTask {
        
        let params: [String : Any] = ["ids" : ids.joined(separator: ",")]
        
        return performRequest("", params: params) {
            completionHandler($0,$2)
        }
    }
    
    /// Perform a search request of a gif from Giphy
    ///
    /// - Parameters:
    ///   - query: The search query.
    ///   - limit: Limit the number of results per page. From 1 to 100.
    ///   - offset: The offset in the number of the results.
    ///   - rating: The max user discretion rating of gifs.
    ///   - completionHandler: Completion handler called once the request is complete.
    /// - Returns: A URLSessionDataTask for the request that has already been resumed.
    @discardableResult func search(_ query: String, limit: Int?, offset: Int?, rating: GIF.Rating?, completionHandler: @escaping ([GIF]?, Response.Pagination?, Error?) -> Void) -> URLSessionDataTask {
        
        var params: [String : Any] = ["q":query as AnyObject]
        
        if let lim = limit {
            params["limit"] = lim
        }
        
        if let off = offset {
            params["offset"] = off
        }
        
        if let rat = rating {
            params["rating"] = rat.rawValue
        }
        
        return performRequest(.search, params: params, completionHandler: completionHandler)
    }
    
    /// Perform a request for the trending gifs.
    ///
    /// - Parameters:
    ///   - limit: Limit the number of results per page. From 1 to 100.
    ///   - offset: The offset in the number of the results
    ///   - rating: The max user discretion rating of gifs.
    ///   - completionHandler: Completion handler called once the request is complete.
    /// - Returns: An NSURLSessionDataTask for the request that has already been resumed.
    @discardableResult func trending(_ limit: Int?, offset: Int?, rating: GIF.Rating?, completionHandler: @escaping ([GIF]?, Response.Pagination?, Error?) -> Void) -> URLSessionDataTask {
        
        var params: [String : Any] = [:]
        if let lim = limit {
            params["limit"] = lim
        }
        if let rat = rating {
            params["rating"] = rat.rawValue
        }
        if let off = offset {
            params["offset"] = off
        }
        
        return performRequest(.trending, params: params, completionHandler: completionHandler)
    }
    
    /// Perform a translate request
    ///
    /// - Parameters:
    ///   - term: The term to translate into a gif
    ///   - rating: The max user discretion rating of gifs
    ///   - completionHandler: Completion handler called once the request is complete
    /// - Returns: A URLSessionDataTask for the request that has already been resumed
    @discardableResult func translate(_ term: String, rating: GIF.Rating?, completionHandler: @escaping (GIF?, Error?) -> Void) -> URLSessionDataTask {
        
        var params: [String : AnyObject] = ["s": term as AnyObject]
        
        if let rat = rating {
            params["rating"] = rat.rawValue as AnyObject?
        }
        
        return performRequest(.translate, params: params) {
            completionHandler($0?.first,$2)
        }
    }
    
    /// Perform a request for a random gif
    ///
    /// - Parameters:
    ///   - tag: Tag that the random gif should have
    ///   - rating: The max user discretion rating of gifs
    ///   - completionHandler: Completion handler called once the request is complete
    /// - Returns: A URLSessionDataTask for the request that has already been resumed
    @discardableResult func random(_ tag: String?, rating: GIF.Rating?, completionHandler: @escaping (GIF?, Error?) -> Void) -> URLSessionDataTask{
        
        var params: [String : Any] = [:]
        if let t = tag {
            params["tag"] = t
        }
        if let rat = rating {
            params["rating"] = rat.rawValue
        }
        
        return performRequest(.random, params: params) {
            completionHandler($0?.first, $2)
        }
    }
    
    private enum Endpoint: String {
        case search
        case trending
        case translate
        case random
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - endpoint: <#endpoint description#>
    ///   - params: <#params description#>
    ///   - completionHandler: <#completionHandler description#>
    /// - Returns: <#return value description#>
    @discardableResult private func performRequest(_ endpoint: String, params: [String: Any]?, completionHandler: @escaping ([GIF]?, Response.Pagination?, Error?) -> Void) -> URLSessionDataTask {
        var params = params ?? [:]
        var urlString = (Giphy.baseURLString as NSString).appendingPathComponent(endpoint)
        
        params["api_key"] = apiKey
        urlString += "?"
        
        for (k, v) in params {
            urlString += k.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            urlString += "="
            urlString += "\(v)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            urlString += "&"
        }
        // Remove extra '&'
        urlString.removeLast()
        let url = URL(string: urlString)!.usingHTTPS
        let dataTask = session.dataTask(with: url) { (data, httpsResponse, error) in
            guard let data = data, let _ = httpsResponse as? HTTPURLResponse else {
                debugPrint(error!.localizedDescription, #file, #line)
                completionHandler(nil,nil, error)
                return
            }
            
            // Decode the response from Giphy
            let decoder = JSONDecoder()
            let response: Response
            do {
                response = try decoder.decode(Response.self, from: data)
            } catch {
                debugPrint(error, #file, #line)
                completionHandler(nil, nil, error)
                return
            }
            
            let metadata = response.meta
            let status: Int = metadata.status
            
            if status != 200 {
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [NSLocalizedDescriptionKey: metadata.message])
                
                completionHandler(nil, nil, error)
                return
            }
            
            let pagination = response.pagination
            
            let gifs = response.data
                        
            completionHandler(gifs, pagination, nil)
        }
        
        dataTask.resume()
        return dataTask
    }
    
    @discardableResult private func performRequest(_ endpoint: Endpoint, params: [String: Any]?, completionHandler: @escaping ([GIF]?, Response.Pagination?, Error?) -> Void) -> URLSessionDataTask {
        return self.performRequest(endpoint.rawValue, params: params, completionHandler: completionHandler)
    }
}

extension URL {
    /// HTTPS version of an HTTP request
    var usingHTTPS: URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: false)!
        components.scheme = "https"
        return components.url!
    }
}
