import Foundation

extension Giphy.Response: CustomStringConvertible {
    var description: String {
        return """
        Pagination
        count: \(String(describing: pagination))
        metadata: \(meta)
        data: \(data)
        """
    }
}
