//
//  GIFCell.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-26.
//  Copyright © 2017 freshworks. All rights reserved.
//

import Foundation
import AVKit

protocol GIFCell: class {
    var contentView: UIView { get }
    weak var playerView: AVPlayerView! { get set }
    var looper: AVPlayerLooper! { get set }
}

extension GIFCell {
    private var player: AVPlayer? {
        get {
            return playerView.playerLayer.player
        }
        set {
            playerView.playerLayer.player = newValue
        }
    }
    
    func setMovie(_ mov: AVPlayerItem) {
        self.player = AVQueuePlayer()
        looper = AVPlayerLooper(player: self.player as! AVQueuePlayer, templateItem: mov)
    }
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func animateFavourite() {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "heart").withRenderingMode(.alwaysTemplate))
        imageView.tintColor = .white
        
        let contentCenter = CGPoint(x: contentView.bounds.midX, y: contentView.bounds.midY)
        let startFrame: CGRect = {
            let startSize = CGSize(width: 50, height: 50)
            let startCenter = CGPoint(x: contentCenter.x - startSize.width/2,
                                      y: contentCenter.y - startSize.height/2)
            return CGRect(origin: startCenter, size: startSize)
        }()
        let endFrame: CGRect = {
            let endSize = CGSize(width: 100, height: 100)
            let endCenter = CGPoint(x: contentCenter.x - endSize.width/2,
                                    y: contentCenter.y - endSize.height/2)
            return CGRect(origin: endCenter, size: endSize)
        }()
        
        imageView.frame = startFrame
        contentView.addSubview(imageView)
        
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
            imageView.frame = endFrame
        }) { (finished) in
            UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [], animations: {
                imageView.alpha = 0
            }) { (finished) in
                imageView.removeFromSuperview()
            }
        }
    }
    
    func animateUnfavourite(completion: @escaping (Bool) -> Void) {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "heart_empty").withRenderingMode(.alwaysTemplate))
        imageView.tintColor = .white
        
        let contentCenter = CGPoint(x: contentView.bounds.midX, y: contentView.bounds.midY)
        let startFrame: CGRect = {
            let startSize = CGSize(width: 50, height: 50)
            let startCenter = CGPoint(x: contentCenter.x - startSize.width/2,
                                      y: contentCenter.y - startSize.height/2)
            return CGRect(origin: startCenter, size: startSize)
        }()
        let endFrame: CGRect = {
            let endSize = CGSize(width: 100, height: 100)
            let endCenter = CGPoint(x: contentCenter.x - endSize.width/2,
                                    y: contentCenter.y - endSize.height/2)
            return CGRect(origin: endCenter, size: endSize)
        }()
        
        imageView.frame = startFrame
        contentView.addSubview(imageView)
        
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
            imageView.frame = endFrame
        }) { (finished) in
            UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [], animations: {
                imageView.alpha = 0
            }, completion: completion)
        }
    }
}
