//
//  MP4CollectionViewCell.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-27.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

class MP4CollectionViewCell: UICollectionViewCell, GIFCell {
    static let nibName = "\(MP4CollectionViewCell.self)"
    static let reuseIdentifier = "\(MP4CollectionViewCell.self)"
    
    @IBOutlet weak var playerView: AVPlayerView!
    var looper: AVPlayerLooper!
}

