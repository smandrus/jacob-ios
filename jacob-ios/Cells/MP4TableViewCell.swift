//
//  MP4TableViewCell.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-26.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

class MP4TableViewCell: UITableViewCell, GIFCell {
    static let nibName = "\(MP4TableViewCell.self)"
    static let reuseIdentifier = "\(MP4TableViewCell.self)"
    
    @IBOutlet weak var playerView: AVPlayerView!
    var looper: AVPlayerLooper!
}
