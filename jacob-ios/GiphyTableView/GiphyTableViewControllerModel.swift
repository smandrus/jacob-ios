//
//  GiphyTableViewControllerModel.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

// TODO: Maybe make this a struct
class GiphyTableViewControllerModel {
    enum ImageQuality {
        case fixedWidth
        case fixedHeight
    }
    
    private let giphy = Giphy(apiKey: giphyUserAPIKey)
    private var foundGiphy: [Giphy.GIF] = []
    
    // MARK: - Pagination
    let pageSize = 10
    private var page: Giphy.Response.Pagination?
    private var cache: [URL: AVPlayerItem] = [:]
//    private lazy var cache: NSCache<NSURL,AVPlayerItem> = {
//        let cache = NSCache<NSURL,AVPlayerItem>()
//        cache.countLimit = 200
//        return cache
//    }()
    
    private let imageSize: ImageQuality
    
    /// <#Description#>
    ///
    /// - Parameter imageSize: <#imageSize description#>
    init(imageQuality: ImageQuality) {
        self.imageSize = imageQuality
    }
    
    // MARK: - Tableview Datasource
    
    /// Number of giphy's in the model
    var count: Int { return foundGiphy.count }
    
    /// <#Description#>
    ///
    /// - Parameter indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func gif(for indexPath: IndexPath) -> AVPlayerItem {
        let url = gifURL(for: indexPath)
        if let gif = cache[url] { //cache.object(forKey: url as NSURL) {
            return gif
        } else {
            let gif = AVPlayerItem(url: url)
            cache[url] = gif //.setObject(gif, forKey: url as NSURL)
            return gif
        }
    }
    
    private func gifURL(for indexPath: IndexPath) -> URL {
        return gifURL(for: indexPath.row)
    }
    
    /// <#Description#>
    ///
    /// - Parameter index: <#index description#>
    /// - Returns: <#return value description#>
    private func gifURL(for index: Int) -> URL {
        let url: URL
        switch self.imageSize {
        case .fixedWidth: url = foundGiphy[index].fixedWidthMetadata.mp4!
        case .fixedHeight: url = foundGiphy[index].fixedHeightMetadata.mp4!
        }
        return url
    }
    
    func aspectRatio(for indexPath: IndexPath) -> CGFloat {
        let width, height: Int
        switch self.imageSize {
        case .fixedWidth:
            height = foundGiphy[indexPath.row].fixedWidthMetadata.height
            width = 200
            
        case .fixedHeight:
            width = foundGiphy[indexPath.row].fixedHeightMetadata.height
            height = 200
        }
        return CGFloat(height) / CGFloat(width)
    }
    
    // MARK: - Trending
    
    /// <#Description#>
    ///
    /// - Parameter completion: <#completion description#>
    func nextTrendingPage(completion: @escaping () -> Void) {
        guard let page = page else {
            // First fetch
            fetchTrending(limit: pageSize, offset: 0, completion: completion)
            return
        }

        // Subsequent fetches
        fetchTrending(limit: pageSize, offset: page.offset + pageSize, completion: completion)
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - limit: <#limit description#>
    ///   - offset: <#offset description#>
    ///   - completion: <#completion description#>
    private func fetchTrending(limit: Int, offset: Int, completion: @escaping () -> Void) {
        self.giphy.trending(limit, offset: offset, rating: nil) { (gifs, page, error) in
            guard let gifs = gifs else {
                debugPrint(error!.localizedDescription, #file, #line)
                return
            }
            
            DispatchQueue.main.async {
                self.page = page
                let mp4s = gifs.filter({ (gif) -> Bool in
                    var url: URL?
                    switch self.imageSize {
                    case .fixedWidth: url = gif.fixedWidthMetadata.mp4
                    case .fixedHeight: url = gif.fixedHeightMetadata.mp4
                    }
                    
                    return url != nil
                })
                self.foundGiphy.append(contentsOf: mp4s)
                completion()
            }
        }
    }
    
    // MARK: - Searching
    
    func clearSearch() {
        self.foundGiphy.removeAll(keepingCapacity: true)
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - query: <#query description#>
    ///   - completion: <#completion description#>
    func nextSearchPage(query: String, completion: @escaping () -> Void) {
        guard let page = page, let total = page.totalCount, total > (page.offset + pageSize) else {
            // First fetch
            fetchSearch(query: query, limit: pageSize, offset: 0, completion: completion)
            return
        }
        // Subsequent fetches
        fetchSearch(query: query, limit: pageSize, offset: page.offset + pageSize, completion: completion)
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - query: <#query description#>
    ///   - limit: <#limit description#>
    ///   - offset: <#offset description#>
    ///   - completion: <#completion description#>
    private func fetchSearch(query: String, limit: Int, offset: Int, completion: @escaping () -> Void) {
        self.giphy.search(query, limit: limit, offset: offset, rating: nil) { (gifs, page, error) in
            guard let gifs = gifs else {
                debugPrint(error!.localizedDescription, #file, #line)
                return
            }
            
            DispatchQueue.main.async {
                self.page = page
                let mp4s = gifs.filter({ (gif) -> Bool in
                    var url: URL?
                    switch self.imageSize {
                    case .fixedWidth: url = gif.fixedWidthMetadata.mp4
                    case .fixedHeight: url = gif.fixedHeightMetadata.mp4
                    }
                    
                    return url != nil
                })
                self.foundGiphy.append(contentsOf: mp4s)
                completion()
            }
        }
    }
    
    // Persistence
    func toggleFavourite(for indexPath: IndexPath) {
        addFavourite(indexPath: indexPath)
//        let url = gifURL(for: indexPath)
//        if Favourites.shared.gifs.contains(url) {
//
//        }
//        let gif = self.gif(for: indexPath)!
//        if Favourites.shared.gifs
//        if Favourites.shared.gifs.contains(where: { $0.1.data == gif.data }) {
//            Favourites.shared.removeFavourite(url: url)
//            return
//        } else {
//            Favourites.shared.addFavourite(url: url, gif: gif)
//        }
    }
    
    func addFavourite(indexPath: IndexPath) {
        let url = gifURL(for: indexPath)
        let asset = gif(for: indexPath).asset
        Favourites.shared.addFavourite(url: url, asset: asset)
    }
}
