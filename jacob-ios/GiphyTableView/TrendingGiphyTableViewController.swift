//
//  TrendingGiphyTableViewController.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-24.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit

class TrendingGiphyTableViewController: BaseGiphyTableViewController {
    
    /// Search controller to help us with filtering.
    var searchController: UISearchController!
    
    /// Secondary search results table view.
    var resultsTableController: ResultsGiphyTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup Search
        resultsTableController = ResultsGiphyTableViewController()
        configureSearchController()
        
        loadNextTrendingPage()
    }
    
    private func configureSearchController() {
        searchController = UISearchController(searchResultsController: resultsTableController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barStyle = .blackTranslucent
        tableView.tableHeaderView = searchController.searchBar
    }
    
    private func loadNextTrendingPage() {
        model.nextTrendingPage {
            let lastRowIndex = self.tableView.numberOfRows(inSection: 0)
            let newIndexPaths = ((lastRowIndex)..<self.model.count).map { IndexPath(row: $0, section: 0) }
            DispatchQueue.main.async {
                self.tableView.insertRows(at: newIndexPaths, with: .none)
            }
        }
    }
    
    // MARK: - Table view data source
    
    // For Pagination
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        if indexPath.row == (model.count - model.pageSize) {
            loadNextTrendingPage()
        }
    }
}

extension TrendingGiphyTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        return
    }
}

// MARK: - UISearchBarDelegate
extension TrendingGiphyTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchController.searchBar.text, !text.isEmpty else { return }
        
        let searchText = text
        let controller = self.resultsTableController!
        controller.search(for: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let controller = self.resultsTableController!
        controller.model.clearSearch()
        controller.tableView.reloadData()
    }
}
