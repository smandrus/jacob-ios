//
//  ResultsGiphyTableViewController.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-24.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit

class ResultsGiphyTableViewController: BaseGiphyTableViewController {
    
    private var query: String!
    
    func search(for query: String) {
        self.query = query.lowercased()
        model.clearSearch()
        loadNextSearchPage()
    }
    
    private func loadNextSearchPage() {
        model.nextSearchPage(query: query) {
            let lastRowIndex = self.tableView.numberOfRows(inSection: 0)            
            let newIndexPaths = ((lastRowIndex)..<self.model.count).map { IndexPath(row: $0, section: 0) }
            
            DispatchQueue.main.async {
                self.tableView.insertRows(at: newIndexPaths, with: .none)
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        if indexPath.row == (model.count - model.pageSize) {
            loadNextSearchPage()
        }
    }
}
