//
//  GiphyTableViewController.swift
//  jacob-ios
//
//  Created by Jacob Christie on 2017-06-23.
//  Copyright © 2017 freshworks. All rights reserved.
//

import UIKit
import AVKit

//* The default items in the table view should be the trending gifs (if nothing in search bar)
//* Contains a search bar at the top.
//* Contains a table view that displays searched gifs.
//* Loading indicator while searching

// Table Cell
//* Should contain a favourite button which allows favouriting and unfavouriting.
//* The favourited items should be stored locally on the device.
class BaseGiphyTableViewController: UITableViewController {
    typealias Model = GiphyTableViewControllerModel
    var model = Model(imageQuality: .fixedWidth)
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(activityIndicatorStyle: .white)
        view.startAnimating()
        view.hidesWhenStopped = true
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        
        tableView.prefetchDataSource = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.backgroundView = activityIndicator

        // Register cell for reuse
        let nib = UINib(nibName: MP4TableViewCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: MP4TableViewCell.reuseIdentifier)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return model.aspectRatio(for: indexPath) * tableView.frame.width
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MP4TableViewCell.reuseIdentifier, for: indexPath) as! MP4TableViewCell
        let item = model.gif(for: indexPath)
        configure(cell: cell, gif: item)
        return cell
    }
    
    func configure(cell: MP4TableViewCell, gif: AVPlayerItem) {
        cell.setMovie(gif)
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didFavouriteCell))
        doubleTapRecognizer.numberOfTapsRequired = 2
        cell.addGestureRecognizer(doubleTapRecognizer)
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? MP4TableViewCell)?.play()
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? MP4TableViewCell)?.pause()
    }
    
    @objc
    func didFavouriteCell(_ gestureRecognizer: UIGestureRecognizer) {
        let cell = gestureRecognizer.view as! MP4TableViewCell
        let indexPath = tableView.indexPath(for: cell)!
        cell.animateFavourite()
        model.toggleFavourite(for: indexPath)
    }
}

extension BaseGiphyTableViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { _ = self.model.gif(for: $0) }
    }
}
